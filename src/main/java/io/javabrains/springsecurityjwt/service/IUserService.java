package io.javabrains.springsecurityjwt.service;

import io.javabrains.springsecurityjwt.exception.CustomException;
import io.javabrains.springsecurityjwt.exception.InvalidException;
import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.request.LoginOrSignupRequest;
import io.javabrains.springsecurityjwt.models.request.SignUpRequest;
import io.javabrains.springsecurityjwt.models.response.APIResponse;
import io.javabrains.springsecurityjwt.models.response.AdminResponse;

import javax.servlet.http.HttpServletResponse;

public interface IUserService {


    APIResponse<Admin> login(LoginOrSignupRequest loginOrSignupRequest, HttpServletResponse response) throws InvalidException, CustomException;

    APIResponse<AdminResponse> signUp(SignUpRequest signUpRequest, HttpServletResponse response) throws Exception;

}
