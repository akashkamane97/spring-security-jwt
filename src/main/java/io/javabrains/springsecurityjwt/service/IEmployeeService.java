package io.javabrains.springsecurityjwt.service;

import io.javabrains.springsecurityjwt.exception.AlreadyExistException;
import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.entity.CommonMedia;
import io.javabrains.springsecurityjwt.models.entity.Employee;
import io.javabrains.springsecurityjwt.models.request.AddUserRequest;
import io.javabrains.springsecurityjwt.models.response.APIResponse;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public interface IEmployeeService {

    public APIResponse<Employee> addEmployee(@RequestBody AddUserRequest request,int adminId) throws Exception;

    APIResponse<Employee> update(AddUserRequest request, HttpServletResponse response);

    String createMedia(CommonMedia commonMedia) throws Exception;
}
