package io.javabrains.springsecurityjwt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javabrains.springsecurityjwt.exception.AlreadyExistException;
import io.javabrains.springsecurityjwt.exception.BaseException;
import io.javabrains.springsecurityjwt.models.entity.AddressEntity;
import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.entity.CommonMedia;
import io.javabrains.springsecurityjwt.models.entity.Employee;
import io.javabrains.springsecurityjwt.models.request.AddUserRequest;
import io.javabrains.springsecurityjwt.models.response.APIResponse;
import io.javabrains.springsecurityjwt.models.response.Address;
import io.javabrains.springsecurityjwt.models.response.EmployeeResponse;
import io.javabrains.springsecurityjwt.repository.AddressRepository;
import io.javabrains.springsecurityjwt.repository.IEmployeeRepository;
import io.javabrains.springsecurityjwt.service.IEmployeeService;
import io.javabrains.springsecurityjwt.util.JwtUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/employee")
public class EmployeeController {


    @Autowired
    IEmployeeService service;

    @Autowired
    IEmployeeRepository repository;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/health")
    public ResponseEntity<String> healthCheck() {
        return ResponseEntity.ok("health check is working");
    }


    @Autowired
    AddressRepository addressRepository;




//    create media

    @PostMapping("/createMedia")
    public ResponseEntity createMedia(@RequestBody CommonMedia commonMedia, String mediaAccessUrl, String storagepath, HttpServletResponse response) throws Exception {
               return ResponseEntity.ok(service.createMedia(commonMedia));
    }





//    get All Employee
    @GetMapping("/employee")
    public ResponseEntity<List<EmployeeResponse>> getemployee() throws JsonProcessingException {
        List<Employee> employees=new ArrayList<>();
        List<EmployeeResponse> employeeResponse1=new ArrayList<>();
        EmployeeResponse employeeResponse=new EmployeeResponse();
        Address addresses=new Address();
        List<Address> address=new ArrayList<>();
        List<Employee> employees1=repository.findAll();

        for(Employee employee: employees1){

            employeeResponse.setId(employee.getId());
            employeeResponse.setFirstname(employee.getFirstname());
            employeeResponse.setLastname(employee.getLastname());
            employeeResponse.setEmail(employee.getEmail());

            List<AddressEntity> address2 =addressRepository.findByEmployee_id(employee.getId());
            for(AddressEntity address1: address2){
                BeanUtils.copyProperties(address1,addresses);
                address.add(addresses);
                employeeResponse.setAddresses(address);

            }
            employeeResponse1.add(employeeResponse);
        }


        return ResponseEntity.ok(employeeResponse1);
    }

//    Add Employee
    @PostMapping("/employee")
    public ResponseEntity<APIResponse<Employee>> addemployee(@RequestHeader(value = "Authorization") String authToken
                                                            ,@RequestBody AddUserRequest request) throws Exception {
        int adminId=getAdminId(authToken);
        return ResponseEntity.ok(service.addEmployee(request,adminId));
    }

//    update Employee
    @PutMapping("/employee")
    public ResponseEntity<APIResponse<Employee>> updateemployee(@RequestBody AddUserRequest request, HttpServletResponse response) {
        return ResponseEntity.ok(service.update(request,response));
    }


//    find employee
    @GetMapping("/employee/{id}")
    public ResponseEntity<Optional<Employee>> findemployee(@RequestHeader(value = "Authorization") String authToken,
                                                            @PathVariable int id) throws Exception {
        return ResponseEntity.ok(repository.findById(id));
    }

// Delete Emplopyee
    @DeleteMapping("/employee")
    public ResponseEntity<List<Employee>> employee(@RequestHeader(value = "Authorization") String authToken) throws Exception {
        repository.deleteById(getUserId(authToken));
        return ResponseEntity.ok(repository.findAll());
    }

    public int getUserId(String authToken) throws Exception {
        if (Objects.nonNull(authToken) && !authToken.isEmpty()) {
//            JwtUtil.validateToken(authToken);
            Map<String, Object> mapData = JwtUtil.getUser(authToken);
            int tokenUserId = (Integer) mapData.get("id");
            return tokenUserId;
        }
        return 0;
    }

    public int getAdminId(String authToken) throws Exception {
        if (Objects.nonNull(authToken) && !authToken.isEmpty()) {
//            JwtUtil.validateToken(authToken);
            Map<String, Object> mapData = JwtUtil.getUserAdmin(authToken);
            int tokenUserId = (Integer) mapData.get("id");
            return tokenUserId;
        }
        return 0;
    }


}