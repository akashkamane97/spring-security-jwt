package io.javabrains.springsecurityjwt.controller;

import io.javabrains.springsecurityjwt.exception.CustomException;
import io.javabrains.springsecurityjwt.exception.InvalidException;
import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.request.LoginOrSignupRequest;
import io.javabrains.springsecurityjwt.models.request.SignUpRequest;
import io.javabrains.springsecurityjwt.models.response.APIResponse;
import io.javabrains.springsecurityjwt.models.response.AdminResponse;
import io.javabrains.springsecurityjwt.service.IUserService;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

//@CrossOrigin
@Slf4j
@Log4j2
@RestController
@RequestMapping("/admin")
class AdminController {

    private static final String controllerTAG = AdminController.class.getSimpleName();
    private static final String serviceName = "admin";

    @Autowired
    private IUserService userService;

    Logger logger = LoggerFactory.getLogger(AdminController.class);

    @RequestMapping("/lombok")
    public String indexs() {
        log.trace("A TRACE Message");
        log.debug("A DEBUG Message");
        log.info("An INFO Message");
        log.warn("A WARN Message");
        log.error("An ERROR Message");

        return "Howdy! Check out the Logs to see the output...";
    }
    @RequestMapping("/")
    public String index() {
        logger.trace("A TRACE Message");
        logger.debug("A DEBUG Message");
        logger.info("An INFO Message");
        logger.warn("A WARN Message");
        logger.error("An ERROR Message");

        return "Howdy! Check out the Logs to see the output...";
    }

    @RequestMapping({"/healthCheck"})
    public String firstPage() {
        return "healthCheck Working";
    }

    /**
     * @return
     * @apiNote fetch all user details by user id
     * @author Akash Kamane
     * @Body LoginOrSignupRequest
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<APIResponse<Admin>> login(@Valid @RequestBody LoginOrSignupRequest loginOrSignupRequest,
                                                    HttpServletResponse response) throws InvalidException, CustomException {
        return ResponseEntity.ok(userService.login(loginOrSignupRequest,response));
    }

    /**
     * @return
     * @apiNote fetch all user details by user id
     * @author Akash Kamane
     * @Body SignUpRequest
     */
    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public ResponseEntity<APIResponse<AdminResponse>> signUp(@RequestBody @Valid SignUpRequest signUpRequest,
                                                             HttpServletResponse response) throws Exception {
        return ResponseEntity.ok(userService.signUp(signUpRequest, response));
    }

}
