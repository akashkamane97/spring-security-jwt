package io.javabrains.springsecurityjwt.repository;

import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.entity.Employee;
import io.javabrains.springsecurityjwt.models.entity.LoginDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginDetailsRepository extends JpaRepository<LoginDetails,Integer> {

    Optional<LoginDetails> findByUserName(String userName);

    boolean existsByUserName(String userName);
}
