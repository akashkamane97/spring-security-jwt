package io.javabrains.springsecurityjwt.repository;

import io.javabrains.springsecurityjwt.models.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, Integer> {

    Optional<Admin> findByEmail(String userName);

//    Optional<Admin> findByUserName(String userName);

    boolean existsByEmail(String userLoginType);

    Admin findByAdminId(int adminid);


}
