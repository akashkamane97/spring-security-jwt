package io.javabrains.springsecurityjwt.repository;

import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.entity.Employee;
import io.javabrains.springsecurityjwt.models.response.Address;
import io.javabrains.springsecurityjwt.models.response.EmployeeResponse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IEmployeeRepository extends JpaRepository<Employee,Integer> {


    Employee findByEmail(String email);

}
