package io.javabrains.springsecurityjwt.repository;

import io.javabrains.springsecurityjwt.models.entity.AddressEntity;
import io.javabrains.springsecurityjwt.models.response.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AddressRepository extends JpaRepository<AddressEntity,Integer> {



    List<AddressEntity> findByEmployee_id(int id);
}
