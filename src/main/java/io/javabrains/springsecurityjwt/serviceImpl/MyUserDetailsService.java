package io.javabrains.springsecurityjwt.serviceImpl;

import io.javabrains.springsecurityjwt.config.CommonUtils;
import io.javabrains.springsecurityjwt.config.Constants;
import io.javabrains.springsecurityjwt.exception.BaseException;
import io.javabrains.springsecurityjwt.exception.CustomException;
import io.javabrains.springsecurityjwt.exception.ExceptionHandler;
import io.javabrains.springsecurityjwt.exception.InvalidException;
import io.javabrains.springsecurityjwt.models.entity.LoginDetails;
import io.javabrains.springsecurityjwt.models.entity.MyUserDetails;
import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.request.LoginOrSignupRequest;
import io.javabrains.springsecurityjwt.models.request.SignUpRequest;
import io.javabrains.springsecurityjwt.models.response.APIResponse;
import io.javabrains.springsecurityjwt.models.response.AdminResponse;
import io.javabrains.springsecurityjwt.models.response.LoginOrSignUpResponse;
import io.javabrains.springsecurityjwt.repository.IEmployeeRepository;
import io.javabrains.springsecurityjwt.repository.AdminRepository;
import io.javabrains.springsecurityjwt.repository.LoginDetailsRepository;
import io.javabrains.springsecurityjwt.service.IUserService;
import io.javabrains.springsecurityjwt.util.JwtUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class MyUserDetailsService implements UserDetailsService, IUserService {

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    LoginDetailsRepository loginDetailsRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private IEmployeeRepository iEmployeeRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Optional<LoginDetails> admin = loginDetailsRepository.findByUserName(userName);
        admin.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));
        return admin.map(MyUserDetails::new).get();

    }


//    public UserDetails loadUserByEmail(String userName) throws UsernameNotFoundException {
//
//        Optional<Admin> admin = adminRepository.findByEmail(userName);
//        admin.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));
//        return admin.map(MyUserDetails::new).get();
//
//    }

    public boolean Authenticate(String username,String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,
                    password)
            );

        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        return true ;
    }


    @Override
    public APIResponse<Admin> login(LoginOrSignupRequest loginOrSignupRequest, HttpServletResponse response) throws InvalidException, CustomException {

        HttpHeaders headers = new HttpHeaders();
        APIResponse responseObj = null;
        LoginOrSignUpResponse loginOrSignUpResponse = null;

        try {

            Authenticate(loginOrSignupRequest.getUserName(),loginOrSignupRequest.getPassword());

            responseObj = new APIResponse<>();
            responseObj.setData(iEmployeeRepository.findAll());
            responseObj.setStatus(true);
            responseObj.setCode(200);
            responseObj.setMessage("sky");
            return responseObj;

        } catch (BaseException e) {

            ExceptionHandler.parseException(e, headers, response);

            responseObj = new APIResponse<>();
            e.printStackTrace();
            responseObj.setCode(400);
            responseObj.setMessage(e.getAppMessage());
            responseObj.setServerStatusCode(e.toString());
            return responseObj;

        }  catch (Exception e) {
            ExceptionHandler.parseException(e, headers, response);
            responseObj = new APIResponse<>();
            responseObj.setCode(500);
            responseObj.setMessage(Constants.OPPO_INTERNAL_ERROR);
//          String txId = saveCommonException("loginOrSignup", loginOrSignupRequest, e, 500);
            responseObj.setServerStatusCode("500");
            e.printStackTrace();
        }
        return responseObj;
    }


    public APIResponse<AdminResponse> signUp(SignUpRequest signUpRequest, HttpServletResponse response) throws Exception {

        HttpHeaders headers = null;
        headers = new HttpHeaders();
        APIResponse<AdminResponse> responseObj = null;
        String jwt = null;
        Map<String, Object> map = null;


        try {


            if (Objects.nonNull(signUpRequest.getUserName())
                    && CommonUtils.isValidEmail(signUpRequest.getUserName())) {

                if (loginDetailsRepository.existsByUserName(signUpRequest.getUserName()))
                    throw new CustomException(Constants.USER_ALREADY_EXISTS, Constants.USER_ALREADY_EXISTS);

            }

            Admin admin = new Admin();
            admin = admin.getAdminResponce(signUpRequest);
            AdminResponse adminResponse1 = new AdminResponse();
            BeanUtils.copyProperties(admin, adminResponse1);
            LoginDetails loginDetails =new LoginDetails();
            loginDetails.setAdmin(admin);
            loginDetails.setUserName(signUpRequest.getUserName());
            adminResponse1.setUserName(signUpRequest.getUserName());
            loginDetails.setPassword(signUpRequest.getPassword());
            loginDetails.setRoles(signUpRequest.getRoles());
            loginDetails.setActive(true);
            Admin admin2 =adminRepository.save(admin);
            adminResponse1.setId(admin2.getAdminId());
//            BeanUtils.copyProperties(loginDetails,signUpRequest);

            loginDetailsRepository.save(loginDetails);
//            Authenticate Admin And Generate Authtoken
            Authenticate(signUpRequest.getUserName(), signUpRequest.getPassword());

            final UserDetails userDetails = loadUserByUsername(signUpRequest.getUserName());

            jwt = jwtTokenUtil.issueToken(adminResponse1, 0);

            map = new ConcurrentHashMap<>();

            map.put("AdminResponse", adminResponse1);
            map.put("AUTH-TOKEN", jwt);

            AdminResponse adminResponse = (AdminResponse) map.get("AdminResponse");

            responseObj = new APIResponse<>();
            responseObj.setData(adminResponse);
            responseObj.setCode(200);
            responseObj.setMessage("REGISTRATION_SUCCESSFULL");
            responseObj.setStatus(true);
            responseObj.setAuthToken((String) map.get("AUTH-TOKEN"));

        } catch (BaseException e) {

            ExceptionHandler.parseException(e, headers, response);

            responseObj = new APIResponse<>();
            e.printStackTrace();
            responseObj.setCode(400);
            responseObj.setMessage(e.getAppMessage());
            responseObj.setServerStatusCode(e.toString());
            return responseObj;

        } catch (Exception e) {

            ExceptionHandler.parseException(e, headers, response);
            e.printStackTrace();
            responseObj = new APIResponse<>();
            responseObj.setCode(500);
            responseObj.setServerStatusCode(e.toString());

            responseObj.setMessage(Constants.OPPO_INTERNAL_ERROR);

            return responseObj;
        }
        return responseObj;
    }

}