package io.javabrains.springsecurityjwt.serviceImpl;

import io.javabrains.springsecurityjwt.config.Constants;
import io.javabrains.springsecurityjwt.exception.AlreadyExistException;
import io.javabrains.springsecurityjwt.exception.BaseException;
import io.javabrains.springsecurityjwt.exception.CustomException;
import io.javabrains.springsecurityjwt.exception.ExceptionHandler;
import io.javabrains.springsecurityjwt.models.entity.AddressEntity;
import io.javabrains.springsecurityjwt.models.entity.Admin;
import io.javabrains.springsecurityjwt.models.entity.CommonMedia;
import io.javabrains.springsecurityjwt.models.entity.Employee;
import io.javabrains.springsecurityjwt.models.request.AddUserRequest;
import io.javabrains.springsecurityjwt.models.response.*;
import io.javabrains.springsecurityjwt.repository.AddressRepository;
import io.javabrains.springsecurityjwt.repository.AdminRepository;
import io.javabrains.springsecurityjwt.repository.IEmployeeRepository;
import io.javabrains.springsecurityjwt.service.IEmployeeService;
import io.javabrains.springsecurityjwt.util.JwtUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class EmployeeServiceImpl implements IEmployeeService {


    @Autowired
    AdminRepository adminRepository;

    @Autowired
    IEmployeeRepository repository;

    @Autowired
    AddressRepository addressRepository;

    @Override
    public APIResponse addEmployee(AddUserRequest request,int adminId) throws Exception {
        APIResponse<EmployeeResponse> apiResponse = null;
        Map<String, Object> map = null;
        EmployeeResponse employeeResponse= new EmployeeResponse();
        Employee employee= null;
        String jwt = null;

        try{
            employee = repository.findByEmail(request.getEmail());

            if(Objects.nonNull(employee)){
                throw new AlreadyExistException("User Already Exist");
            }
            employee= new Employee();

//          get the admin id from the token
//            check wheather admin is present in the table or not


            Admin a = adminRepository.findByAdminId(adminId);
//            List<Address> address=addressRepository.findAll();

            if(Objects.isNull(a)){

                throw new AuthenticationException("Only admin can Add employee");

            }

            employee = employee.setEmployee(request);
            employee.setAdmin(a);
            Employee saveEmp=repository.save(employee);

            employeeResponse.setId(saveEmp.getId());
            employeeResponse.setFirstname(saveEmp.getFirstname());
            employeeResponse.setLastname(saveEmp.getLastname());
            employeeResponse.setEmail(saveEmp.getEmail());
            Address address=request.getAddress();
            List<Address> addresses=new ArrayList<>();
            addresses.add(address);
            AddressEntity addressEntity=new AddressEntity();
            BeanUtils.copyProperties(address,addressEntity);

            addressEntity.setEmployee(saveEmp);
            addressRepository.save(addressEntity);
            employeeResponse.setAddresses(addresses);
//            employeeResponse.setAddressResponse(saveEmp.getAddresses());
            employeeResponse.setAdmin(saveEmp.getAdmin().getAdminId());

//            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//            Date date = (Date)formatter.parse(saveEmp.getDateOfBirth().toString());
//
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(date);
//            String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" +cal.get(Calendar.YEAR);
//
//            employeeResponse.setDateOfBirth(formatter.format(formatedDate));


            employeeResponse.setDateOfBirth(saveEmp.getDateOfBirth());


            jwt = JwtUtil.issueTokenEmp(saveEmp, 0);

            map = new ConcurrentHashMap<>();

            map.put("EmployeeResponse", employeeResponse);
            map.put("AUTH-TOKEN", jwt);

            apiResponse = new APIResponse<>();
            apiResponse.setCode(200);
            apiResponse.setStatus(true);
            apiResponse.setData(employeeResponse);
            apiResponse.setAuthToken((String) map.get("AUTH-TOKEN"));

            return apiResponse;

        }catch (BaseException e){
            apiResponse = new APIResponse<>();
            apiResponse.setCode(400);
            apiResponse.setMessage(e.getAppMessage());
            apiResponse.setServerStatusCode(e.toString());
            e.printStackTrace();
            return apiResponse;
        }


        catch (Exception e){
           e.printStackTrace();
            apiResponse = new APIResponse<>();
            apiResponse.setCode(500);
            apiResponse.setMessage(e.getMessage());
            apiResponse.setServerStatusCode(e.toString());

            return apiResponse;
        }

    }

    @Override
    public APIResponse<Employee> update(AddUserRequest request, HttpServletResponse response) {
        APIResponse<Employee> responseObj = null;
        HttpHeaders headers = new HttpHeaders();

        Employee employee = null;
        try {
            if (Objects.nonNull(request)) {
                employee = repository.findByEmail(request.getEmail());

                if (Objects.nonNull(employee)) {
                    BeanUtils.copyProperties(request, employee);
                    repository.save(employee);
                    responseObj = new APIResponse<>();
                    responseObj.setData(employee);
                    responseObj.setCode(200);
                    responseObj.setMessage("User Updated Successfully");
                    return responseObj;
                } else throw new CustomException("User is Not Present");
            }
        } catch (BaseException e) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            ExceptionHandler.parseException(e, headers, response);
            e.printStackTrace();
            responseObj = new APIResponse<>();
            responseObj.setMessage("User is Not Present");
            responseObj.setCode(400);
            responseObj.setServerStatusCode(e.toString());
            return responseObj;

        } catch (Exception e) {
            ExceptionHandler.parseException(e, headers, response);
            e.printStackTrace();
            responseObj = new APIResponse<>();
            responseObj.setMessage("Something went wrong");
            responseObj.setCode(500);
            responseObj.setServerStatusCode(e.toString());
            e.printStackTrace();
        }

        return responseObj;
    }

    @Override
    public String createMedia(CommonMedia commonMedia) throws Exception {
        CommonMediaResponse commonMediaResponse;

        try {

            if (Objects.nonNull(commonMedia)) {

                commonMedia.setMediaAccessUrl(commonMedia.getMediaAccessUrl());
                commonMedia.setMediaStoragePath(commonMedia.getMediaStoragePath());
                commonMedia.setRedirectType(commonMedia.getRedirectType());
                commonMedia.setRedirectUrl(commonMedia.getRedirectUrl());
                commonMedia.setUserid(commonMedia.getId());
                commonMedia.setType(commonMedia.getType());
                commonMedia.setMediaType(commonMedia.getMediaType());
//                commonMedia.setCmsMediaId(commonMedia.getId());
//                commonMedia.setBannerFlag(BANNERFLAG.IMAGE);
                commonMedia.setId(null);
            }
//            commonMediaResponse = commonMediaRepository.save(commonMedia).getCommonMedia();

//            if (Objects.nonNull(commonMediaResponse)) {
//
//                if (commonMedia.getType() == CommonMediaType.HC) {
//
//                    return Constants.INSURANCE_BANNER_ADDED;
//                }
//                return Constants.BANNER_CREATED;
//            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
}




