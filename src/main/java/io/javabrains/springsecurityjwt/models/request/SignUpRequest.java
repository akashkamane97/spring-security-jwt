package io.javabrains.springsecurityjwt.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public final class SignUpRequest {

    @NotEmpty(message = "userName is mandatory")
    private String userName;

    @NotEmpty(message = "password is mandatory")
    private String password;

    @NotEmpty(message = "fname is mandatory")
    private String fname;

    @NotEmpty(message = "lname is mandatory")
    private String lname;

    @Column(name = "mname", columnDefinition = "text")
    private String mname;

    @Column(name = "email", unique = true, columnDefinition = "varchar(1000)")
    private String email;

    @Column(name = "city", length = 255)
    private String city;

    @Column(name = "address", length = 255)
    private String address;

//    @Temporal(TemporalType.DATE)
//    private Date dateOfBirth;
    private String dateOfBirth;

    private String mobile;
    private boolean active=true;
    private String roles="ROLE_USER";
    private String socialType;
}
