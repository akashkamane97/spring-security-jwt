package io.javabrains.springsecurityjwt.models.request;

import io.javabrains.springsecurityjwt.models.response.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddUserRequest {

    private String firstname;
    private String lastname;
    private String email;
    private String city;
    private Date dateOfBirth;
    private Address address;
}
