package io.javabrains.springsecurityjwt.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
@Data
@AllArgsConstructor
@NoArgsConstructor

public final class LoginOrSignupRequest {

	@NotEmpty(message = "userName is mandatory")
	private String userName;

	@NotEmpty(message = "password is mandatory")
	private String password;
}