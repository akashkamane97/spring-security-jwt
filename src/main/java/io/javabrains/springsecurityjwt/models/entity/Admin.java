package io.javabrains.springsecurityjwt.models.entity;

import io.javabrains.springsecurityjwt.config.CommonUtils;
import io.javabrains.springsecurityjwt.models.request.SignUpRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADMIN_TABLE")
public class Admin extends BaseDomain{

//    firstname,lastname,password,address,dob,company etc.


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private int adminId;

    @Column(name = "firstname", columnDefinition = "text")
    private String firstname;

    @Column(name = "lastname", columnDefinition = "text")
    private String lastname;

    @Column(name = "email", unique = true, columnDefinition = "varchar(1000)")
    private String email;


//    @Temporal(TemporalType.DATE)
//    private Date dateOfBirth;
    private String dateOfBirth;

    private String company;
//    private String userName;
//    private String password;
    private boolean active;
//    private String roles;

    @OneToMany(mappedBy="admin", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Employee> employees;

    public Admin getAdminResponce(SignUpRequest signUpRequest){
        Admin admin = new Admin();
        admin.setFirstname(signUpRequest.getFname());
        admin.setLastname(signUpRequest.getLname());
        admin.setActive(true);
//        admin.setUserName(signUpRequest.getUserName());
//        admin.setRoles(signUpRequest.getRoles());
//      CommonUtils.isPassWordAsPerPolicy(signUpRequest.getPassword());
//        admin.setPassword(signUpRequest.getPassword());
        if (Objects.nonNull(signUpRequest.getEmail())
                && CommonUtils.isValidEmail(signUpRequest.getEmail())) {
            admin.setEmail(signUpRequest.getEmail());
        }
        admin.setDateOfBirth(signUpRequest.getDateOfBirth());
        return admin;
    }




}
