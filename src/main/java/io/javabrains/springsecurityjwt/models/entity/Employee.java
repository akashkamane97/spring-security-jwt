package io.javabrains.springsecurityjwt.models.entity;

import com.fasterxml.jackson.annotation.*;
import io.javabrains.springsecurityjwt.models.request.AddUserRequest;
import io.javabrains.springsecurityjwt.models.response.EmployeeResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "employee")
public class Employee extends BaseDomain implements Serializable {

//    empId,firstname,lastname,address,dob,mobile,city etc.


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "employee_Id")
    private int id;

    @Column(name = "firstname", columnDefinition = "text")
    private String firstname;

    @Column(name = "lastname", columnDefinition = "text")
    private String lastname;

    @Column(name = "email", unique = true, columnDefinition = "varchar(1000)")
    private String email;



    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

//    @Fetch(value= FetchMode.SELECT)


    @OneToMany(mappedBy="employee",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<AddressEntity> address;


    //    @JoinColumn(name="adminId",nullable=false,insertable=false)
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Admin admin;


    public EmployeeResponse setEmployeeResponse(AddUserRequest request) {
        EmployeeResponse employeeResponse=new EmployeeResponse();
        employeeResponse.setFirstname(request.getFirstname());
        employeeResponse.setLastname(request.getLastname());
        employeeResponse.setEmail(request.getEmail());
        employeeResponse.setCity(request.getCity());
        employeeResponse.setDateOfBirth(request.getDateOfBirth());


        return employeeResponse;
    }

    public Employee setEmployee(AddUserRequest request) {
        Employee employeeResponse=new Employee();
        employeeResponse.setFirstname(request.getFirstname());
        employeeResponse.setLastname(request.getLastname());
        employeeResponse.setEmail(request.getEmail());
        employeeResponse.setDateOfBirth(request.getDateOfBirth());
        return employeeResponse;
    }
}
