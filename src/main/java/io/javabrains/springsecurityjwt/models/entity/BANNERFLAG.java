package io.javabrains.springsecurityjwt.models.entity;

public enum BANNERFLAG {

    VIDEO("video",0),IMAGE("image",1);

    private final String type;
    private final int value;

    BANNERFLAG(String type, int value){

        this.type = type;
        this.value = value;
    }
}
