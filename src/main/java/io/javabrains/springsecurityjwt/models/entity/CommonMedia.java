package io.javabrains.springsecurityjwt.models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="common_media")
public class CommonMedia extends BaseDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    private String mediaType;

    private String mediaSize;

    private String mediaText;

    private String mediaStoragePath;

    private String redirectType;

    private String redirectUrl;

    private String sequence;

    @Column(columnDefinition = "longtext")
    private String mediaAccessUrl;

    private int userid;

    @Enumerated(EnumType.STRING)
    BANNERFLAG bannerFlag;

    @Enumerated(EnumType.STRING)
    private CommonMediaType type;

}
