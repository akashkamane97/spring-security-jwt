package io.javabrains.springsecurityjwt.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.javabrains.springsecurityjwt.models.response.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Address")
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer addressId;

    private String contry;
    private String city;

//    @OneToMany(mappedBy="Address", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
//    private Set<Address> addresses;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Employee employee;

    public Address getAddress() {
        Address address = new Address();
        address.setId(getAddressId());
        address.setCity(getCity());
        address.setContry(getContry());

        return address;
    }

}
