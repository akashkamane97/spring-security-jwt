package io.javabrains.springsecurityjwt.models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity  extends BaseDomain{


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "firstname", columnDefinition = "text")
    private String firstname;

    @Column(name = "lastname", columnDefinition = "text")
    private String lastname;

    @Column(name = "email", unique = true, columnDefinition = "varchar(1000)")
    private String email;


    //    @Temporal(TemporalType.DATE)
//    private Date dateOfBirth;
    private String dateOfBirth;

    private String company;
    private String userName;
    private String password;
    private boolean active;
    private String roles;
    private String AdminOFEmployee;
}
