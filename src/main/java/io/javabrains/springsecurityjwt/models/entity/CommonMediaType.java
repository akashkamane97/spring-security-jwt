package io.javabrains.springsecurityjwt.models.entity;

public enum CommonMediaType {

    MAIN("MAIN", 19);


    private final String type;
    private final int value;

    CommonMediaType(String type, int value) {
        this.type = type;
        this.value = value;
    }
}
