package io.javabrains.springsecurityjwt.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonMediaResponse {
    private Integer id;

    private String mediaType;

    private String mediaText;

    private String mediaAccessUrl;

    private String redirectType;

    private String redirectUrl;

    private int userId;

    private String type;

}
