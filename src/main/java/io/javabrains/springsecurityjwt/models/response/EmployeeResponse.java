package io.javabrains.springsecurityjwt.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.javabrains.springsecurityjwt.models.entity.Admin;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeResponse {

    private int id;
    private String firstname;
    private String lastname;
    private String email;
    private String city;

    private Date dateOfBirth;
    private int admin;
    private List<Address> addresses;

}
