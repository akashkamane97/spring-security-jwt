package io.javabrains.springsecurityjwt.models.response;

public final class LoginOrSignUpResponse {
	
	private boolean newUser ;
	private String message;

	public boolean isNewUser() {
		return newUser;
	}

	public void setNewUser(boolean newUser) {
		this.newUser = newUser;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

