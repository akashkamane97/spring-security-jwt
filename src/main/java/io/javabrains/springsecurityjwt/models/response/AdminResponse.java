package io.javabrains.springsecurityjwt.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminResponse {

    private int id;
    private String firstname;
    private String lastname;
    private String email;
    private Date dateOfBirth;
    private String company;
    private String userName;
    private String password;
    private boolean active;
    private String roles;
}
