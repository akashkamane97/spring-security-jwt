package io.javabrains.springsecurityjwt.models.response;

public class APIResponse<T> {

	private T data;
	private Integer code;
	private String message;
	private boolean status;
	private String authToken;
	private String serverStatusCode;

	public String getServerStatusCode() {
		return serverStatusCode;
	}

	public void setServerStatusCode(String serverStatusCode) {
		this.serverStatusCode = serverStatusCode;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public T getData() {
	        return this.data;
	    }

	    public void setData(T data) {
	        this.data = data;
	    }

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getAuthToken() {
			return authToken;
		}

		public void setAuthToken(String authToken) {
			this.authToken = authToken;
		}


	 
	}

