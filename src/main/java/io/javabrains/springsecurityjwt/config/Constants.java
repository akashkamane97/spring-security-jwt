package io.javabrains.springsecurityjwt.config;

public final class Constants {

    public static final String OPPO_INTERNAL_ERROR = "Looks like there's some problem loading the page, please visit us after 10 mins.";
    public static final String INVALID_REQUEST = "Invalid Request!";
    public static final String USER_NOT_FOUND = "User Not Register";
    public static final String INVALID_USER_TOKEN = "You have been logged out for security reason";

    public static final String INPUT_PARAMS_NOT_READABLE = "Input parameters not readable!";
    public static final String UNAUTHORIZED_ACCESS = "Unauthorized access!";
    public static final String UNAUTHORIZED_TOKEN = "Unauthorized Token!";
    public static final String NOT_LOGGED_IN_PLEASE_LOGIN = "You have not logged in, please login ";

    public static final String ALL_DETAILS = "Please Enter All Details";
    public static final String USER_ALREADY_EXISTS = "User already exists";

    public static final String INVALID_EMAIL_ID_OR_MOBILE_NUMBER = "Invalid Email ID or Mobile Number";

    public static final String SUCCESS = "Success";

    public static final String PASSWORD_NOT_PER_POLICY ="Password is not as per password policy";
    public static final String FAIL = "Fail";

    public static final String PAYMENT_NOT_DONE = "your payment is not yet complete";

    public static final String ICICIFormat = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String appFormat = "dd/MM/yyyy";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final String MM_DD_YYYY = "MM/dd/yyyy";
}




