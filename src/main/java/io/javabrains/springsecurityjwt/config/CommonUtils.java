package io.javabrains.springsecurityjwt.config;

import io.javabrains.springsecurityjwt.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public final class CommonUtils {


    @Autowired
    RestTemplateBuilder restTemplateBuilder;

    public static boolean isValidEmail(String userLoginType) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return userLoginType.matches(regex);
    }

    public static boolean isValidMobileNo(String userLoginType) {
//            Pattern p = Pattern.compile("(0/91)?[6-9][0-9]{9}");
//            Matcher m = p.matcher(userLoginType);
//            return (m.find() && m.group().equals(userLoginType));
        return true;
    }


    public static void isPassWordAsPerPolicy(String password) throws Exception {

        String specialCharacters = "-@%\\[\\}+'!/#$^?:;,\\(\"\\)~`.*=&\\{>\\]<_";

        String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[" + specialCharacters + "])(?=\\S+$).{8,15}$";


        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(password);

        if (!m.matches()) {

            throw new CustomException(Constants.PASSWORD_NOT_PER_POLICY);
        }


    }
}
