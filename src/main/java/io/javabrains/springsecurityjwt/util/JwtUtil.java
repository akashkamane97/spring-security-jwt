package io.javabrains.springsecurityjwt.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xml.internal.security.algorithms.SignatureAlgorithm;
import io.javabrains.springsecurityjwt.config.Constants;
import io.javabrains.springsecurityjwt.exception.CustomException;
import io.javabrains.springsecurityjwt.models.entity.Employee;
import io.javabrains.springsecurityjwt.models.response.AdminResponse;
import io.javabrains.springsecurityjwt.models.response.LoginOrSignUpResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;

@Service
public class JwtUtil {

    private static String SECRET_KEY = "secret";
    private static String APP_USER = "appuser";
    private static String APP_ADMIN = "appadmin";

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getUsername());
    }

//  add parameter  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(io.jsonwebtoken.SignatureAlgorithm.valueOf(SignatureAlgorithm.getDefaultPrefix(SECRET_KEY)), SECRET_KEY).compact();
    }



    public static String issueToken(AdminResponse adminResponse, long days) {

        // The JWT signature algorithm we will be using to sign the token
//        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        // We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
//        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
//        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJCEProviderName());

        // Adding only the essential properties of User in JWT Toke n
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("id", adminResponse.getId());
        userMap.put("userName", adminResponse.getUserName());
        userMap.put("email", adminResponse.getEmail());
        userMap.put("logInUserType", APP_ADMIN);


        // Let's set the JWT Claims
//        JwtBuilder builder = Jwts.builder().claim("userMap", userMap).setId(String.valueOf(adminResponse.getId()))
//                .setIssuedAt(now).signWith(signatureAlgorithm, signingKey);

        // if it has been specified, let's add the expiration
        if (days > 0) {
            ZoneId defaultZoneId = ZoneId.systemDefault();
            LocalDate localDate =  LocalDate.now().plusDays(days);
            Date expiryDate = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
//            long expMillis = nowMillis + (1000 * 60 * 2 );
//            Date exp = new Date(expMillis);
//            builder.setExpiration(expiryDate);
        }
        // Builds the JWT and serializes it to a compact, URL-safe string
//        return builder.compact();
        return null;
    }
    public static String issueTokenEmp(Employee emp, long days) {

        // The JWT signature algorithm we will be using to sign the token
//        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        // We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
//        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJCEProviderName());

        // Adding only the essential properties of User in JWT Toke n
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("id", emp.getId());
        userMap.put("userName", emp.getFirstname());
        userMap.put("email", emp.getEmail());
        userMap.put("logInUserType", APP_USER);


        // Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().claim("userMap", userMap).setId(String.valueOf(emp.getId()))
                .setIssuedAt(now);
//                .setIssuedAt(now).signWith(signatureAlgorithm, signingKey);

        // if it has been specified, let's add the expiration
        if (days > 0) {
            ZoneId defaultZoneId = ZoneId.systemDefault();
            LocalDate localDate =  LocalDate.now().plusDays(days);
            Date expiryDate = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
//            long expMillis = nowMillis + (1000 * 60 * 2 );
//            Date exp = new Date(expMillis);
            builder.setExpiration(expiryDate);
        }
        // Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }


    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
//        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
        return (username.equals(userDetails.getUsername()));
    }
    public static Map<String, Object> getUser(String token) throws Exception {

        try {

            Map<String, Object> userDetailsMap = null;

            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                    .parseClaimsJws(token).getBody();

            if (claims.containsKey("userMap")) {

                ObjectMapper mapper = new ObjectMapper();

                userDetailsMap = mapper.convertValue(claims.get("userMap", LinkedHashMap.class), Map.class);
                if (!Objects.toString(userDetailsMap.get("logInUserType")).equalsIgnoreCase(APP_USER)) {
                    throw new CustomException(Constants.UNAUTHORIZED_TOKEN);
                }

            } else
                throw new CustomException(Constants.UNAUTHORIZED_TOKEN);

            return userDetailsMap;

        } catch (Exception ex) {
            throw ex;
        }
    }

    public static Map<String, Object> getUserAdmin(String token) throws Exception {

        try {

            Map<String, Object> userDetailsMap = null;

            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                    .parseClaimsJws(token).getBody();

            if (claims.containsKey("userMap")) {

                ObjectMapper mapper = new ObjectMapper();

                userDetailsMap = mapper.convertValue(claims.get("userMap", LinkedHashMap.class), Map.class);
                if (!Objects.toString(userDetailsMap.get("logInUserType")).equalsIgnoreCase(APP_ADMIN)) {

                    throw new CustomException(Constants.UNAUTHORIZED_TOKEN);
                }

            } else
                throw new CustomException(Constants.UNAUTHORIZED_TOKEN);

            return userDetailsMap;

        } catch (Exception ex) {
            throw ex;
        }
    }






}