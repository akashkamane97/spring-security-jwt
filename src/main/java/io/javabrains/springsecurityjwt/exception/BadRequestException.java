package io.javabrains.springsecurityjwt.exception;

public  final class BadRequestException extends BaseException {

	    private static final int code = 4004;
	    private static final String appMessage = "Request is Empty";
	    private static final String message ="Request is Empty";

	    public BadRequestException() {
	        super(code, message, appMessage);
	    }

	    public BadRequestException(String message) {
	        super(code, message, appMessage);
	    }

	    public BadRequestException(String message, String appMessage) {
	        super(code, message, appMessage);
	    }
	
}
