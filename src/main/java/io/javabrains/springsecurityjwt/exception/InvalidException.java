package io.javabrains.springsecurityjwt.exception;

public final class InvalidException extends BaseException{
	
	   public InvalidException(Integer code, String message, String appMessage) {
		super(code, message, appMessage);
	}

	private static final int code = 200;
	    private static final String appMessage = "Invalid Details";
	    private static final String message = "Invalid Details";

	    public InvalidException() {
	        super(code, message, appMessage);
	    }

	    public InvalidException(String message) {
	        super(code, message, appMessage);
	    }

	    public InvalidException(String message, String appMessage) {
	        super(code, message, appMessage);
	    }
	}
