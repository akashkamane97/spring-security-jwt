package io.javabrains.springsecurityjwt.exception;

public final class AlreadyExistException extends BaseException{

    private static final int code = 315;
    private static final String appMessage = "Already exist";
    private static final String message ="Already exist";

    public AlreadyExistException() {
        super(code, message, appMessage);
    }

    public AlreadyExistException(String message) {
        super(code, message, appMessage);
    }

    public AlreadyExistException(String message, String appMessage) {
        super(code, message, appMessage);
    }
}



