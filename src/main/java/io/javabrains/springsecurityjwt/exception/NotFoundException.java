package io.javabrains.springsecurityjwt.exception;

public final class NotFoundException extends BaseException {

	   private static final int code = 4004;
	    private static final String appMessage = "Not Found";
	    private static final String message ="Not Found";

	    public NotFoundException() {
	        super(code, message, appMessage);
	    }

	    public NotFoundException(String message) {
	        super(code, message, appMessage);
	    }

	    public NotFoundException(String message, String appMessage) {
	        super(code, message, appMessage);
	    }
	
}
