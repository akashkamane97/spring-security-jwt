package io.javabrains.springsecurityjwt.exception;

import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;

public class ExceptionHandler {
	
	 public static final void parseException(BaseException ex, HttpHeaders headers, HttpServletResponse response) {

	        if(ex instanceof AlreadyExistException) {

	            headers.set("Status-code", "315");
	            headers.set("DEBUG-MSG", ex.getMessage());
	            headers.set("APP-MSG", ex.getAppMessage());

	            response.setStatus(200);

	        } 
	        
	        if(ex instanceof InvalidException) {

	            headers.set("Status-code", "400");
	            headers.set("DEBUG-MSG", ex.getMessage());
	            headers.set("APP-MSG", ex.getAppMessage());

	            response.setStatus(200);

	        }
	        if(ex instanceof NotFoundException) {

	            headers.set("Status-code", "400");
	            headers.set("DEBUG-MSG", ex.getMessage());
	            headers.set("APP-MSG", ex.getAppMessage());

	            response.setStatus(200);

	        }
	        if(ex instanceof BadRequestException) {

	            headers.set("Status-code", "4000");
	            headers.set("DEBUG-MSG", ex.getMessage());
	            headers.set("APP-MSG", ex.getAppMessage());

	            response.setStatus(400);

	        }
	 }
	 
	 public static final void parseException(Exception ex, HttpHeaders headers, HttpServletResponse response) {

	        headers.set("x-response-code","Internal Exception");
	        headers.set("x-debug-msg", ex.getMessage());

	        response.setStatus(500);
	    }
}
